from bidict import bidict
import re

morsecodeMap = bidict({'A':'.-','B':'-...','C':'-.-.','D':'-..','E':'.','F':'..-.','G':'--.','H':'....','I':'..','J':'.---','K':'-.-','L':'.-..','M':'--'
,'N':'-.','O':'---','P':'.--.','Q':'--.-','R':'.-.','S':'...','T':'-','U':'..-','V':'...-','W':'.--','X':'-..-','Y':'-.--','Z':'--..','0':'-----' ,'1':'.----'
,'2':'..---','3':'...--','4':'....-' ,'5':'.....','6':'-....' ,'7':'--...','8':'---..','9':'----.','.':'.-.-.-'})

class MorseDecoder: 
    def translateString(self,words):
        message = ""
        for word in words:
            for letter in word.split(" "):
                if letter not in morsecodeMap.inverse:
                    raise Exception('Code not found. Available encoding follows '+str([*morsecodeMap.inverse.keys()]))
                message += morsecodeMap.inverse[letter]
            message += " "
        return message

    def translate2Human(self,morseString):
        words = re.compile("\s{3,}").split(morseString)
        words = [w for w in words if len(w) > 0]
        return self.translateString(words)


    def encodeMorse(self,text):
        words = re.compile("\s+").split(text.upper())
        message = ""
        for word in words:
            for letter in word:
                if letter not in morsecodeMap:
                    raise Exception("'" + letter + "'  is not defined in tranlation table")
                message += morsecodeMap[letter]
                message += " "
            message += "   "
        return message

if __name__ == "__main__":
    try:
       decoder = MorseDecoder()
       print(decoder.translate2Human(".... --- .-.. .-   -- . .-.. .."))
       print(decoder.encodeMorse("HOLAMELI"))
    except Exception as ex:
        print('Error parsing', ex.args)