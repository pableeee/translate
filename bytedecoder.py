import bidict
from enum import Enum
from collections import defaultdict
from itertools import groupby


class MorseCode(Enum):
    # byte encoding, Encoding,  time unit leght
    DOT = ('1', ".", 1)
    DASH = ('1', "-", 3)
    ABSENCE = ('0', "", 1)
    SEPARATOR = ('0', " ", 3)
    SPACE = ('0', "   ", 7)


class MorseCodeByteDecoder:
    def __init__(self):
        pass

    # returns a tuple with the bits an its count as they appear in the stream
    def parseBinaryMessage(self, text):
        i = 0
        groups = groupby(text)
        parsed = [ (label , sum ( 1 for _ in group )) for label, group in groups ]
        mm = defaultdict(lambda:defaultdict(int))
        for ele in parsed:
            char = ele[i]
            mm[ele[0]][ele[1]] +=1

        return parsed, mm

    # finds the "closest" representation of a code, given a set of bits
    def closestCandidate(self, num, arr):
        curr = None
        for index in arr:
            if index == num:
                return index
            if self.hasMinorDistance(num, curr, index):
                curr = index
            elif (abs(num - index) == abs(num - curr)):
                # in case the diff is the same, the number largest prevails since the encoding error porcentage-wise is smaller
                curr = max(curr, index)
        return curr

    def hasMinorDistance(self, num, curr, index):
        return curr is None or (abs(num - index) < abs(num - curr))

    def convertToMorseCode(self, segmentTuple, codeMap):
        subMap = codeMap[segmentTuple[0]]
        return subMap[self.closestCandidate(segmentTuple[1], subMap.keys())]

    # tries to identify the freecuency used during the binary encoding
    # encoding errors larger than a time unit, could cause problems
    def evaluateBestSpeedOption(self,mapCount):
        maxScore = 0
        bestOption = -1
        for length in mapCount.keys():
            mm = defaultdict(int)
            for ele in mapCount.keys():
                div = ele // length
                mm[div] +=1
            score = mm[1] + mm[3]
            if score >= maxScore:
                maxScore = score
                bestOption = length
        return bestOption

    # decodes a stream of bits into morse code
    def decodeBits2Morse(self, inpt):
        parsed, mapCount = self.parseBinaryMessage(inpt)
        self.codeMap = defaultdict(lambda: defaultdict(str))
        if '1' not in mapCount:
            return ""

        if parsed[0][0] == '0':
            parsed=parsed[1:]
        if parsed[len(parsed) - 1][0] == '0':
            parsed=parsed[:-1]

        minHigh = self.evaluateBestSpeedOption(mapCount['1'])
        for code in MorseCode:
            self.codeMap[code.value[0]][minHigh * code.value[2]] = code.value[1]

        message = ""
        for e in parsed:
            message += self.convertToMorseCode(e, self.codeMap)
        return message