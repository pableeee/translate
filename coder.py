from sys import stdin
from bytedecoder import MorseCodeByteDecoder
from morsedecoder import MorseDecoder
import getopt
import sys


byteDecoder = MorseCodeByteDecoder()
morseDecoder = MorseDecoder()

def printUsage():
    print ('Usage:',sys.argv[0],' ')
    print ('\t-b: to decode bit stream')
    print ('\t-m: to encode plaintext in morse code')
    print ('Input will be read from stdin')
    
if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'bmh')

        if len(opts) != 1:
            print('Either -b or -m parameter needs to be provided')
            printUsage()
            exit(1)

        mode = opts[0][0]
        if mode[0] == '-h':
            printUsage()
            exit(0)

        for line in stdin:
            line=line.strip()

            if mode == '-b':
                message = byteDecoder.decodeBits2Morse(line)
                print(message)
            elif  mode == '-m':
                message = morseDecoder.translate2Human(line)
                print(message)
    except getopt.GetoptError:
        print ('Unrecognized parameter')
        print ('Usage:',sys.argv[0],' ')
    except Exception as ex:
        print('Error parsing', ex.args)

    exit(0)