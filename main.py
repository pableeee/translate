import flask
from flask import request, Response
from flask import jsonify
from flask_restful import abort
import json
from morsedecoder import MorseDecoder

app = flask.Flask(__name__)

decoder = MorseDecoder()

class RequestError(Exception):
    def __init__(self,response):
        self.response = response

def buildResponse(message):
    output = {'response': message}
    return Response(
        mimetype="application/json",
        response=json.dumps(output),
        status=200
    )

def buildErrorResponse(status_code, message):
    response = jsonify({
        'status': status_code,
        'message': message})
    response.status_code = status_code
    return response


def validateRequest(req):
    if req.content_length <= 0:
        #abort(400, message="text property is missing")
        raise RequestError(buildErrorResponse(400,"No Content"))

    jsonRequest = req.get_json()

    if 'text' not in jsonRequest:
        raise RequestError(buildErrorResponse(400,"text property is missing in request"))

    return jsonRequest

@app.route('/2text', methods=['POST'])
def translate2text():
    try:
        jsonRequest = validateRequest(request)
        message = decoder.translate2Human(jsonRequest['text'])
        return buildResponse(message.strip())
    except RequestError as ex:
        return ex.response
    except Exception as ex:
        return buildErrorResponse(417,str(ex))  

@app.route('/2morse', methods=['POST'])
def translate2morse():
    try:
        jsonRequest = validateRequest(request)
        message = decoder.encodeMorse(jsonRequest['text'].upper())
        return buildResponse(message.strip())
    except RequestError as ex:
        return ex.response
    except Exception as ex:
        return buildErrorResponse(417,str(ex))  

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
