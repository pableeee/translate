import unittest
import random
from bytedecoder import MorseCodeByteDecoder

'''
This test class, encode in a binary a set of predefined messages, but including some "noise" in the encoding.
The noise introduces in a given char encoding, is of 1 time unit (2 time units could create a problem distinguishing . & - on frec = 2bits ). 
'''


class TestStringMethods(unittest.TestCase):
    decoder = MorseCodeByteDecoder()
 
    def generateBinaryStream(self,morseMessage,speed):
        result = []
        words = morseMessage.split(" ")
        for i in range(0,len(words)):
            word = words[i]
            letters = word.split("*")
            for e in range(0,len(letters)):
                letter = letters[e]
                for c in range(0,len(letter)):
                    code = letter[c]
                    if code == '.':
                        result.append(('1',1*speed))
                    elif code == '-':
                        result.append(('1',3*speed))
                    if c < len(letter) - 1:
                        result.append(('0',1*speed))
                if e < len(letters) - 1:
                    result.append(('0',3*speed))
            if i < len(words) - 1:
                    result.append(('0',7*speed))
        return result

    def getByteStreamWithNoise(self, speed, code):
        bytesStream = self.generateBinaryStream(code,speed)
        message = ""
        for tup in bytesStream:
            noise = (speed//2) * random.randint(-1,1)
            nextValue =( tup[1] + noise )
            message +=  tup[0] * nextValue
        return message

    def formatOutputForComparison(self, code):
        return code.strip().replace(" ","   ").replace("*"," ")


    def test_hola_meli(self):
        #text = "HOLA MELI"
        code="....*---*.-..*.- --*.*.-..*.."
        finalPatterToMatch = self.formatOutputForComparison(code)
        for i in range(2,6):
            message = self.getByteStreamWithNoise(i,code)
            res=self.decoder.decodeBits2Morse(message)
            self.assertTrue(res.strip() == finalPatterToMatch)


    def test_hola_cryptii(self):
        #text = "HOLA MELI"
        code="        -.-.*.-.*..*.--.*-*..*.. ..*... .-*-. ---*.--.*.*-.*...*---*..-*.-.*-.-.*. .--*.*-... .-*.--.*.--.*.-..*..*-.-.*.-*-*..*---*-."
        finalPatterToMatch = self.formatOutputForComparison(code)
        for i in range(2,6):
            message = self.getByteStreamWithNoise(i,code)
            res=self.decoder.decodeBits2Morse(message)
            self.assertTrue(res.strip() == finalPatterToMatch)


    def test_examen_tecnico(self):
        #text = "HOLA MELI"
        code="       .*-..-*.-*--*.*-. -*.*-.-.*-.*..*-.-.*--- --*.*.-.*-.-.*.-*-..*---*.-..*..*-...*.-.*."
        finalPatterToMatch = self.formatOutputForComparison(code)
        for i in range(2,6):
            message = self.getByteStreamWithNoise(i,code)
            res=self.decoder.decodeBits2Morse(message)
            self.assertTrue(res.strip() == finalPatterToMatch)

            
if __name__ == '__main__':
    unittest.main()
    
