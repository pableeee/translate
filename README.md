# Translate

This is a simple utility to translate from/to morse code. It consists of two distinct applications.

* A command line tool, `coder.py` , that can translate from a stream bits to morse code, or from morse code to textplain.
* A Web service that translates from / to morse code.

# Considerations

The following time units definition is used to decode the bit steam. Any difference in more tha 1 time unit could result in a decoding error (this result depends on the sign and encoding frequency).

Sign | Time duration (in time units)
------------ | -------------
dot (.) | 1
dash (-) | 3
After Sign absence | 1
Letter separator | 3
Word separator ( space ) | 7

# CLI Tool
CLI tool can be use to tranlate a bit of stream to morse code, using the `-b` parameter. The input is read from the stdin. Below is an example.
```
cat tests/bitstream.txt | python coder.py -b
.... --- .-.. .- -- . .-.. ..
.... --- .-.. .-   -- . .-.. ..
.... --- .-.. .-   -- . .-.. ..
.... --- .-.. .-   -- . .-.. ..
.... --- .-.. .-   -- . .-.. ..
```

In a similar fashion, the CLI tool can be used to tranlate a morse code encoded message into plaintext. The input is read from the stdin.

```
cat tests/plaintext.txt | python coder.py -m
HOLA MELI
CRIPTII IS AN OPENSOURCE WEB APPLICATION
EXAMEN TECNICO MERCADOLIBRE
```



# Web Service

The two following endpoints, would translate from morse code to text and encode text to morse code respectively.

https://quickstart-1568523968447.appspot.com/2text

https://quickstart-1568523968447.appspot.com/2morse

The service expects `JSON` format in the `POST` requests, and specifically a `text` attribute containing the stream to decode/encode. 

`Content-Type: application/json` in the request header is also required.

Following are two sample executions using the curl command.

```
$  curl -i -H "Content-Type: application/json" -X POST "https://quickstart-1568523968447.appspot.com/2text" -d '{"text": ".... --- .-.. .-   -- . .-.. .."}'

HTTP/1.1 200 OK
Content-Type: application/json
Vary: Accept-Encoding
X-Cloud-Trace-Context: d2b4714b637bfed64231fd2119ae9bca;o=1
Date: Tue, 21 Jan 2020 01:23:21 GMT
Server: Google Frontend
Content-Length: 25
Alt-Svc: quic=":443"; ma=2592000; v="46,43",h3-Q050=":443"; ma=2592000,h3-Q049=":443"; ma=2592000,h3-Q048=":443"; ma=2592000,h3-Q046=":443"; ma=2592000,h3-Q043=":443"; ma=2592000

{"response": "HOLA MELI"}
```

```
$   curl -i -H "Content-Type: application/json" -X POST "https://quickstart-1568523968447.appspot.com/2morse" -d '{"text": "Hola Meli"}'
HTTP/1.1 200 OK
Content-Type: application/json
Vary: Accept-Encoding
X-Cloud-Trace-Context: 59fe9a99597f77e83137d3feb77cd83f;o=1
Date: Tue, 21 Jan 2020 01:24:19 GMT
Server: Google Frontend
Content-Length: 48
Alt-Svc: quic=":443"; ma=2592000; v="46,43",h3-Q050=":443"; ma=2592000,h3-Q049=":443"; ma=2592000,h3-Q048=":443"; ma=2592000,h3-Q046=":443"; ma=2592000,h3-Q043=":443"; ma=2592000

{"response": ".... --- .-.. .-    -- . .-.. .."}
```
